package challenge;

import java.util.ArrayList;
import java.util.Collections;

public class DataSekolah implements InterfaceDataSekolah {
	private String kelas;
	private ArrayList<Integer> nilai;
	
	public DataSekolah(ArrayList<Integer> nilai) {
		super();
		this.nilai = nilai;
	}

	public String getKelas() {
		return kelas;
	}

	public void setKelas(String kelas) {
		this.kelas = kelas;
	}

	public ArrayList<Integer> getNilai() {
		return nilai;
	}

	public void setNilai(ArrayList<Integer> nilai) {
		this.nilai = nilai;
	}

	@Override
	public double countMean(ArrayList<Integer> nilai) {
		double sum = 0;
		for (int i = 0; i < nilai.size(); i++) {
			sum += nilai.get(i);
		}
		return sum / nilai.size();

	}

	@Override
	public double countMedian(ArrayList<Integer> nilai) {
		Collections.sort(nilai);
		double median;
		if (nilai.size() % 2 == 0) {
			median = (nilai.get(nilai.size() / 2) + nilai.get(nilai.size() / 2 - 1)) / 2;
		} else {
			median = nilai.get(nilai.size() / 2);
		}
		return median;

	}

	@Override
	public int countModus(ArrayList<Integer> nilai) {
		int nilaiTerbanyak = 0, jumlahTerbanyak = 0;

		for (int i = 0; i < nilai.size(); i++) {
			int count = 0;
			for (int j = 0; j < nilai.size(); j++) {
				if (nilai.get(j) == nilai.get(i)) {
					++count;
				}
			}
			if (count > jumlahTerbanyak) {
				jumlahTerbanyak = count;
				nilaiTerbanyak = nilai.get(i);
			}
		}
		return nilaiTerbanyak;
	}

	@Override
	public int countFrek(ArrayList<Integer> nilai, int batasBawah, int batasAtas) {
		int count = 0;
		for (int i = 0; i < nilai.size(); i++) {
			if (nilai.get(i) >= batasBawah && nilai.get(i) <= batasAtas) {
				count++;
			}
		}
		return count;
	}
}
