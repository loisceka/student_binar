package challenge;

import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Collectors;

public class DataSekolah implements InterfaceDataSekolah {

	private String kelas;
	private ArrayList<Integer> nilai;

	public DataSekolah(ArrayList<Integer> nilai) {
		super();
		this.nilai = nilai;
	}

	public DataSekolah() {

	}

	public String getKelas() {
		return kelas;
	}

	public void setKelas(String kelas) {
		this.kelas = kelas;
	}

	public ArrayList<Integer> getNilai() {
		return nilai;
	}

	public void setNilai(ArrayList<Integer> nilai) {
		this.nilai = nilai;
	}

	@Override
	public double countMean(ArrayList<Integer> nilai) {
		double sum = nilai.stream().collect(Collectors.averagingDouble(n -> n));
		return sum;
	}

	@Override
	public double countMedian(ArrayList<Integer> nilai) {
		Collections.sort(nilai);
		double median = nilai.get(nilai.size() / 2);
		if (nilai.size() % 2 == 0) {
			median = (median + nilai.get(nilai.size() / 2 - 1)) / 2;
		}
		return median;
	}

	@Override
	public int countModus(ArrayList<Integer> nilai) {
		int nilaiTerbanyak = 0, jumlahTerbanyak = 0;

		for (int i = 0; i < nilai.size(); i++) {
			int count = 0;
			for (int j = 0; j < nilai.size(); j++) {
				if (nilai.get(j) == nilai.get(i)) {
					++count;
				}
			}
			if (count > jumlahTerbanyak) {
				jumlahTerbanyak = count;
				nilaiTerbanyak = nilai.get(i);
			}
		}
		return nilaiTerbanyak;
	}

	@Override
	public int countFrek(ArrayList<Integer> nilai, int batasBawah, int batasAtas) {
		int count = 0;
		for (int i = 0; i < nilai.size(); i++) {
			if (nilai.get(i) >= batasBawah && nilai.get(i) <= batasAtas) {
				count++;
			}
		}
		return count;
	}
}
