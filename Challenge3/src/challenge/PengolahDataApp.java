package challenge;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class PengolahDataApp {
	public static void main(String[] args) {
		ArrayList<Integer> listDatas = read("C:\\temp\\direktori\\data_sekolah.csv");
		DataSekolah data = new DataSekolah(listDatas);
		Scanner scan = new Scanner(System.in);
		int pil;
		while (true) {
			menu();
			System.out.print("Masukkan Pilihan: ");
			pil = scan.nextInt();
			switch (pil) {
			case 1:
				System.out.println("----------------------------------------");
				System.out.println("Aplikasi Pengolah Nilai Siswa");
				System.out.println("----------------------------------------");
				case1(data, listDatas);
				System.out.println();
				System.out.println("0. Exit");
				System.out.println("1. Kembali Ke Menu Utama");
				System.out.print("Masukkan Pilihan : ");
				pil = scan.nextInt();
				if (pil == 0) {
					System.exit(0);
				} else if (pil == 1) {
					break;
				} else {
					System.out.println("Masukkan Pilihan Yang Sesuai");
				}
			case 2:
				System.out.println("----------------------------------------");
				System.out.println("Aplikasi Pengolah Nilai Siswa");
				System.out.println("----------------------------------------");
				case2(data, listDatas);
				System.out.println();
				System.out.println("0. Exit");
				System.out.println("1. Kembali Ke Menu Utama");
				System.out.print("Masukkan Pilihan : ");
				pil = scan.nextInt();
				if (pil == 0) {
					System.exit(0);
				} else if (pil == 1) {
					break;
				} else {
					System.out.println("Masukkan Pilihan Yang Sesuai");
				}
			case 3:
				System.out.println("----------------------------------------");
				System.out.println("Aplikasi Pengolah Nilai Siswa");
				System.out.println("----------------------------------------");
				case1(data, listDatas);
				case2(data, listDatas);
				System.out.println();
				System.out.println("0. Exit");
				System.out.println("1. Kembali Ke Menu Utama");
				System.out.print("Masukkan Pilihan : ");
				pil = scan.nextInt();
				if (pil == 0) {
					System.exit(0);
				} else if (pil == 1) {
					break;
				} else {
					System.out.println("Masukkan Pilihan Yang Sesuai");
				}
			case 0:
				System.exit(0);
			default:
				System.out.println("Masukkan Pilihan Yang Sesuai!");
			}
		}
	}

	public static ArrayList<Integer> read(String csvFile) {
		ArrayList<Integer> listData = new ArrayList<>();
		try {
			File file = new File(csvFile);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line = "";
			String[] tempArr;
			while ((line = br.readLine()) != null) {
				tempArr = line.split(";");
				for (int i = 1; i < tempArr.length; i++) {
					listData.add(Integer.valueOf(tempArr[i]));
				}
			}
			br.close();
		} catch (IOException ioe) {
			System.out.println("----------------------------------------");
			System.out.println("Aplikasi Pengolah Nilai Siswa");
			System.out.println("----------------------------------------");
			System.out.println("File Tidak Ditemukan");
			ioe.printStackTrace();
		}
		return listData;
	}

	public static void write(String txtFile, String content) {
		try {
			File file = new File(txtFile);
			if (content == null)
				throw new IllegalArgumentException("Content tidak boleh kosong");
			if (file.createNewFile()) {
				System.out.println("File Telah Digenerate di C://temp/direktori");
				System.out.println("Silahkan Dicek...");
			}
			FileWriter writer = new FileWriter(file);
			BufferedWriter bwr = new BufferedWriter(writer);
			bwr.write(content);
			bwr.newLine();
			bwr.flush();
			bwr.close();
		} catch (IOException ioe) {
			System.out.println("Terdapat error...");
			ioe.printStackTrace();
		}
	}

	public static void case1(DataSekolah data, ArrayList<Integer> listDatas) {
		String isi = "Berikut Hasil Pengolahan Data Frekuensi Nilai : \n" + "\nNilai               |     Frekuensi"
				+ "\nKurang Dari 6       |     " + data.countFrek(listDatas, 0, 5) + "\n6                   |     "
				+ data.countFrek(listDatas, 6, 6) + "\n7                   |     " + data.countFrek(listDatas, 7, 7)
				+ "\n8                   |     " + data.countFrek(listDatas, 8, 8) + "\n9                   |     "
				+ data.countFrek(listDatas, 9, 9) + "\n10                  |     " + data.countFrek(listDatas, 10, 10);
		write("C:\\temp\\direktori\\data_sekolah_frekuensi_nilai.txt", isi);
	}

	public static void case2(DataSekolah data, ArrayList<Integer> listDatas) {
		String isi = "Berikut Hasil Pengolahan Data Sebaran Nilai : \n" + "\nMean : " + data.countMean(listDatas)
				+ "\nMedian : " + data.countMedian(listDatas) + "\nModus : " + data.countModus(listDatas);
		write("C:\\temp\\direktori\\data_sekolah_mean_median_modus.txt", isi);
	}

	public static void menu() {
		System.out.println("----------------------------------------");
		System.out.println("Aplikasi Pengolah Nilai Siswa");
		System.out.println("----------------------------------------");
		System.out.println("Letakan file csv dengan nama data_sekolah di direktori berikut");
		System.out.println("direktori : c://temp/direktori");
		System.out.println();
		System.out.println("Pilih Menu !");
		System.out.println("1. Generate Txt Untuk Menampilkan Frekuensi Nilai");
		System.out.println("2. Generate Txt Untuk Menampilkan Mean, Median dan Modus");
		System.out.println("3. Generate Kedua File");
		System.out.println("0. Exit");
	}

}
