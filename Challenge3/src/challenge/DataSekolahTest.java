package challenge;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class DataSekolahTest {

	@Test
	@DisplayName("Positive Test Case - Count Median")
	void testCountMed() {
		ArrayList<Integer> testData = new ArrayList<>();
		testData.add(5);
		testData.add(6);
		testData.add(7);
		testData.add(8);
		testData.add(8);
		DataSekolah dataSekolah = new DataSekolah();
		double hasil = dataSekolah.countMedian(testData);
		Assertions.assertEquals(7, hasil);
	}

	@Test
	@DisplayName("Positive Test Case - Count Mean")
	void testCountMean() {
		ArrayList<Integer> testData = new ArrayList<>();
		testData.add(5);
		testData.add(6);
		testData.add(7);
		testData.add(8);
		testData.add(8);
		DataSekolah dataSekolah = new DataSekolah();
		double hasil = dataSekolah.countMean(testData);
		Assertions.assertEquals(6.8, hasil);
	}

	@Test
	@DisplayName("Positive Test Case - Count Modus")
	void testCountModus() {
		ArrayList<Integer> testData = new ArrayList<>();
		testData.add(5);
		testData.add(6);
		testData.add(7);
		testData.add(8);
		testData.add(8);
		DataSekolah dataSekolah = new DataSekolah();
		int hasil = dataSekolah.countModus(testData);
		Assertions.assertEquals(8, hasil);
	}

	@Test
	@DisplayName("Positive Test Case - Count Frequency n > 7")
	void testCountFreq() {
		ArrayList<Integer> testData = new ArrayList<>();
		testData.add(5);
		testData.add(6);
		testData.add(7);
		testData.add(8);
		testData.add(8);
		DataSekolah dataSekolah = new DataSekolah();
		int hasil = dataSekolah.countFrek(testData, 8, 10);
		Assertions.assertEquals(2, hasil);
	}
	
	@Test
	@DisplayName("Negative Test Case - Count Median False")
	void testNegativeCountMed() {
		ArrayList<Integer> testData = new ArrayList<>();
		testData.add(5);
		testData.add(6);
		testData.add(7);
		testData.add(8);
		testData.add(8);
		DataSekolah dataSekolah = new DataSekolah();
		double hasil = dataSekolah.countMedian(testData);
		Assertions.assertEquals(10, hasil);
	}

	@Test
	@DisplayName("Negative Test Case - Count Mean False")
	void testNegativeCountMean() {
		ArrayList<Integer> testData = new ArrayList<>();
		testData.add(5);
		testData.add(6);
		testData.add(7);
		testData.add(8);
		testData.add(8);
		DataSekolah dataSekolah = new DataSekolah();
		double hasil = dataSekolah.countMean(testData);
		Assertions.assertEquals(5, hasil);
	}

}
