package challenge;

import java.util.ArrayList;

public interface InterfaceDataSekolah {

	double countMean(ArrayList<Integer> nilai);

	double countMedian(ArrayList<Integer> nilai);

	int countModus(ArrayList<Integer> nilai);

	int countFrek(ArrayList<Integer> nilai, int batasBawah, int batasAtas);
}
