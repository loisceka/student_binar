package com.binar.challenge.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.binar.challenge.models.Booking;
import com.binar.challenge.repositories.BookingRepository;

@Service
public class BookingService {

	@Autowired
	private BookingRepository bookingRepository;

	public List<Booking> getAll() {
		return bookingRepository.findAll();
	}

	public Booking getById(Integer id) {
		return bookingRepository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Booking not found"));
	}

	public Booking create(Booking booking) {
		if (booking.getId() != null) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Booking already exist");
		}
		return bookingRepository.save(booking);
	}

	public Booking update(Integer id, Booking booking) {
		Booking oldBookingData = getById(id);
		booking.setId(oldBookingData.getId());

		return bookingRepository.save(booking);
	}

	public Booking delete(Integer id) {
		Booking booking = getById(id);

		bookingRepository.deleteById(id);
		return booking;
	}
}
