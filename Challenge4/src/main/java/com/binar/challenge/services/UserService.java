package com.binar.challenge.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.binar.challenge.models.User;
import com.binar.challenge.repositories.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public List<User> getAll() {
		return userRepository.findAll();
	}

	public User getById(Integer id) {
		return userRepository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found"));
	}

	public User create(User user) {
		if (user.getId() != null) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "User already exist");
		}
		return userRepository.save(user);
	}

	public User update(Integer id, User user) {
		User oldUserData = getById(id);
		user.setId(oldUserData.getId());

		return userRepository.save(user);
	}

	public User delete(Integer id) {
		User user = getById(id);

		userRepository.deleteById(id);
		return user;
	}
}
