package com.binar.challenge.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.binar.challenge.models.Seat;
import com.binar.challenge.repositories.SeatRepository;

@Service
public class SeatService {

	@Autowired
	private SeatRepository seatRepository;

	public List<Seat> getAll() {
		return seatRepository.findAll();
	}

	public Seat getById(Integer id) {
		return seatRepository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Seat not found"));
	}

	public Seat create(Seat seat) {
		if (seat.getId() != null) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Seat already exist");
		}
		return seatRepository.save(seat);
	}

	public Seat update(Integer id, Seat seat) {
		Seat oldSeatData = getById(id);
		seat.setId(oldSeatData.getId());

		return seatRepository.save(seat);
	}

	public Seat delete(Integer id) {
		Seat seat = getById(id);

		seatRepository.deleteById(id);
		return seat;
	}
}
