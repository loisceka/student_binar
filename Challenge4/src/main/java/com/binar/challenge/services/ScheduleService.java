package com.binar.challenge.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.binar.challenge.models.Film;
import com.binar.challenge.models.Schedule;
import com.binar.challenge.repositories.FilmRepository;
import com.binar.challenge.repositories.ScheduleRepository;

@Service
public class ScheduleService {

	@Autowired
	private ScheduleRepository scheduleRepository;
	@Autowired
	private FilmRepository filmRepository;

	public List<Schedule> getAll() {
		return scheduleRepository.findAll();
	}

	public Schedule getById(Integer id) {
		return scheduleRepository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Schedule not found"));
	}

	public Schedule create(Schedule schedule) {
		if (schedule.getId() != null) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Schedule already exist");
		}
		return scheduleRepository.save(schedule);
	}

	public Schedule update(Integer id, Schedule schedule) {
		Schedule oldScheduleData = getById(id);
		schedule.setId(oldScheduleData.getId());

		return scheduleRepository.save(schedule);
	}

	public Schedule delete(Integer id) {
		Schedule schedule = getById(id);

		scheduleRepository.deleteById(id);
		return schedule;
	}

	public Schedule findByFilm(String name) {
		Film filmData = filmRepository.findByName(name);
		return scheduleRepository.findByFilmId(filmData);
	}
}
