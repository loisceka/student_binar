package com.binar.challenge.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.binar.challenge.models.Film;
import com.binar.challenge.repositories.FilmRepository;

@Service
public class FilmService {

	@Autowired
	private FilmRepository filmRepository;

	public List<Film> getAll() {
		return filmRepository.findAll();
	}

	public Film getById(Integer id) {
		return filmRepository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Film not found"));
	}

	public Film create(Film film) {
		if (film.getId() != null) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Film already exist");
		}
		return filmRepository.save(film);
	}

	public Film update(Integer id, Film film) {
		Film oldFilmData = getById(id);
		film.setId(oldFilmData.getId());

		return filmRepository.save(film);
	}

	public Film delete(Integer id) {
		Film film = getById(id);

		filmRepository.deleteById(id);
		return film;
	}

	public List<Film> getByAired() {
		return filmRepository.findByAired(true);
	}
}
