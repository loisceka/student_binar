package com.binar.challenge.models;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Schedule")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Schedule {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "schedule_id")
	private Integer id;

	@Column(name = "date_aired")
	private Date dateAired;

	@Column(name = "start_time")
	private Date startTime;

	@Column(name = "end_time")
	private Date endTime;

	@Column(name = "price")
	private Double price;

	@JoinColumn(name = "film_id", referencedColumnName = "film_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private Film filmId;

	public Schedule(Date dateAired, Date startTime, Date endTime, Double price, Film filmId) {
		super();
		this.dateAired = dateAired;
		this.startTime = startTime;
		this.endTime = endTime;
		this.price = price;
		this.filmId = filmId;
	}
}
