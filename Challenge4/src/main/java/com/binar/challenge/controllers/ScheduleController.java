package com.binar.challenge.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.binar.challenge.models.Film;
import com.binar.challenge.models.Schedule;
import com.binar.challenge.services.ScheduleService;

@RestController
@RequestMapping("/schedule")
public class ScheduleController {

	@Autowired
	private ScheduleService scheduleService;

	@GetMapping
	public ResponseEntity<List<Schedule>> getAll() {
		return new ResponseEntity<>(scheduleService.getAll(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Schedule> getById(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(scheduleService.getById(id), HttpStatus.OK);
	}

	@GetMapping("/search")
	public ResponseEntity<Schedule> getById(@RequestBody Film film) {
		return new ResponseEntity<>(scheduleService.findByFilm(film.getName()), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Schedule> create(@RequestBody Schedule schedule) {
		return new ResponseEntity<>(scheduleService.create(schedule), HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Schedule> update(@PathVariable("id") Integer id, @RequestBody Schedule schedule) {
		return new ResponseEntity<>(scheduleService.update(id, schedule), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Schedule> delete(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(scheduleService.delete(id), HttpStatus.OK);
	}
}
