package com.binar.challenge.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.binar.challenge.models.Seat;
import com.binar.challenge.services.SeatService;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/seat")
public class SeatController {

	@Autowired
	private SeatService seatService;

	@GetMapping
	public ResponseEntity<List<Seat>> getAll() {
		return new ResponseEntity<>(seatService.getAll(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Seat> getById(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(seatService.getById(id), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Seat> create(@RequestBody Seat seat) {
		return new ResponseEntity<>(seatService.create(seat), HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Seat> update(@PathVariable("id") Integer id, @RequestBody Seat seat) {
		return new ResponseEntity<>(seatService.update(id, seat), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Seat> delete(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(seatService.delete(id), HttpStatus.OK);
	}
}
