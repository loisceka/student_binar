package com.binar.challenge.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.binar.challenge.models.Film;
import com.binar.challenge.models.Schedule;

@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Integer> {

	Schedule findByFilmId(Film filmId);
}
