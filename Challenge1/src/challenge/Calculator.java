package challenge;

import java.util.Scanner;

public class Calculator {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int pil;
		while (true) {
			menu1();
			System.out.print("Masukkan Pilihan: ");
			pil = scan.nextInt();
			switch (pil) {
			case 1:
				menu2();
				System.out.print("Masukkan Pilihan: ");
				pil = scan.nextInt();
				if (pil == 1) {
					System.out.println("----------------------------------------");
					System.out.println("Anda Memilih Persegi");
					System.out.println("----------------------------------------");
					System.out.print("Masukkan Panjang Sisi: ");
					double panjang = scan.nextDouble();
					double hasil = panjang * panjang;
					System.out.println("");
					System.out.println("Processing...");
					System.out.println("");
					System.out.println("Luas dari Persegi adalah : " + hasil);
					System.out.println("----------------------------------------");
					System.out.println("Tekan apa saja untuk kembali ke menu sebelumnya");

				} else if (pil == 2) {
					System.out.println("----------------------------------------");
					System.out.println("Anda Memilih Lingkaran");
					System.out.println("----------------------------------------");
					System.out.print("Masukkan Panjang Jari-jari : ");
					double jari = scan.nextDouble();
					double hasil = (3.14 * jari * jari);
					System.out.println("");
					System.out.println("Processing...");
					System.out.println("");
					System.out.println("Luas dari Lingkaran adalah : " + hasil);
					System.out.println("----------------------------------------");
					System.out.println("Tekan apa saja untuk kembali ke menu sebelumnya");
				} else if (pil == 3) {
					System.out.println("----------------------------------------");
					System.out.println("Anda Memilih Segitiga");
					System.out.println("----------------------------------------");
					System.out.print("Masukkan Alas : ");
					double alas = scan.nextDouble();
					System.out.println("Masukkan Tinggi : ");
					double tinggi = scan.nextDouble();
					double hasil = (alas * tinggi) / 2;
					System.out.println("");
					System.out.println("Processing...");
					System.out.println("");
					System.out.println("Luas dari Persegi Panjang adalah : " + hasil);
					System.out.println("----------------------------------------");
					System.out.println("Tekan apa saja untuk kembali ke menu sebelumnya");
				} else if (pil == 4) {
					System.out.println("----------------------------------------");
					System.out.println("Anda Memilih Persegi Panjang");
					System.out.println("----------------------------------------");
					System.out.print("Masukkan Panjang : ");
					double panjang = scan.nextDouble();
					System.out.print("Masukkan Lebar : ");
					double lebar = scan.nextDouble();
					double hasil = panjang * lebar;
					System.out.println("");
					System.out.println("Processing...");
					System.out.println("");
					System.out.println("Luas dari Persegi Panjang adalah : " + hasil);
					System.out.println("----------------------------------------");
					System.out.println("Tekan apa saja untuk kembali ke menu sebelumnya");
				}
				break;
			case 2:
				menu3();
				System.out.print("Masukkan Pilihan: ");
				pil = scan.nextInt();
				if (pil == 1) {
					System.out.println("----------------------------------------");
					System.out.println("Anda Memilih Kubus");
					System.out.println("----------------------------------------");
					System.out.print("Masukkan Panjang Sisi: ");
					double panjang = scan.nextDouble();
					double hasil = panjang * panjang * panjang;
					System.out.println("");
					System.out.println("Processing...");
					System.out.println("");
					System.out.println("Volum dari Kubus adalah : " + hasil);
					System.out.println("----------------------------------------");
					System.out.println("Tekan apa saja untuk kembali ke menu sebelumnya");
				} else if (pil == 2) {
					System.out.println("----------------------------------------");
					System.out.println("Anda Memilih Balok");
					System.out.println("----------------------------------------");
					System.out.print("Masukkan Panjang : ");
					double panjang = scan.nextDouble();
					System.out.print("Masukkan Lebar : ");
					double lebar = scan.nextDouble();
					System.out.print("Masukkan Tinggi : ");
					double tinggi = scan.nextDouble();
					double hasil = panjang * lebar * tinggi;
					System.out.println("");
					System.out.println("Processing...");
					System.out.println("");
					System.out.println("Volum dari Balok adalah : " + hasil);
					System.out.println("----------------------------------------");
					System.out.println("Tekan apa saja untuk kembali ke menu sebelumnya");
				} else if (pil == 3) {
					System.out.println("----------------------------------------");
					System.out.println("Anda Memilih Tabung");
					System.out.println("----------------------------------------");
					System.out.print("Masukkan Panjang Jari-jari : ");
					double jari = scan.nextDouble();
					double hasil = (3.14 * jari * jari);
					System.out.println("");
					System.out.println("Processing...");
					System.out.println("");
					System.out.println("Volum dari Balok adalah : " + hasil);
					System.out.println("----------------------------------------");
					System.out.println("Tekan apa saja untuk kembali ke menu sebelumnya");
				}
				break;
			case 0:
				System.exit(0);
			default:
				System.out.println("Masukkan Pilihan Yang Sesuai!");
			}
		}
	}

	public static void menu1() {
		System.out.println("----------------------------------------");
		System.out.println("Kalkulator Penghitung Luas dan Volum");
		System.out.println("----------------------------------------");
		System.out.println("Menu");
		System.out.println("1. Hitung Luas Bidang");
		System.out.println("2. Hitung Volum");
		System.out.println("0. Tutup Aplikasi");
		System.out.println("----------------------------------------");
	}

	public static void menu2() {
		System.out.println("----------------------------------------");
		System.out.println("Pilih Bidang yang akan dihitung");
		System.out.println("----------------------------------------");
		System.out.println("1. Persegi");
		System.out.println("2. Lingkaran");
		System.out.println("3. Segitiga");
		System.out.println("4. Persegi Panjang");
		System.out.println("0. Kembali ke Menu Sebelumnya");
		System.out.println("----------------------------------------");
	}

	public static void menu3() {
		System.out.println("----------------------------------------");
		System.out.println("Pilih Bidang yang akan dihitung");
		System.out.println("----------------------------------------");
		System.out.println("1. Kubus");
		System.out.println("2. Balok");
		System.out.println("3. Tabung");
		System.out.println("0. Kembali ke Menu Sebelumnya");
		System.out.println("----------------------------------------");
	}

}
